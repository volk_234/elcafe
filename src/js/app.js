import {initMap} from './map';
import 'bootstrap/js/dist/tab'
import AOS from 'aos/dist/aos.js'
import IMask from 'imask';
import jQuery from "jquery";
window.$ = window.jQuery = jQuery;
import Swiper from 'swiper'

//init google map
window.initMap = initMap;

//sliders initialization
let swipers = document.querySelectorAll('.swiper-container');
for (let element of swipers) {
    new Swiper(element, {
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 'auto',
        coverflowEffect: {
            rotate: 0,
            stretch: 0,
            depth: 200,
            modifier: 1,
        },
        observer: true,
        observeParents: true,
        navigation: {
            nextEl: element.querySelector('.navigation--right'),
            prevEl: element.querySelector('.navigation--left'),
        },
        breakpoints: {
            767: {
                slidesPerView: 1,
                spaceBetween: 10
            },
        }
    });
}

//scroll animations initialization
AOS.init();

//attach background to mouse
let lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;
let bgSplash = $('.bg-splash__image-bg');

function moveBackground() {
    x += (lFollowX - x) * friction;
    y += (lFollowY - y) * friction;

    let translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

    bgSplash.css({
        '-webit-transform': translate,
        '-moz-transform': translate,
        'transform': translate
    });

    window.requestAnimationFrame(moveBackground);
}

$(window).on('mousemove click', function (e) {
    let lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
    let lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
    lFollowX = (20 * lMouseX) / 100;
    lFollowY = (10 * lMouseY) / 100;
});
moveBackground();

//cup animation
let cups = $('.cup__layers img');
let cupOffsetY = $(cups[1]).offset().top;

$(window).on('scroll', function (e) {
    let passedScroll = 1 - (cupOffsetY - window.scrollY) / window.outerHeight;
    passedScroll = passedScroll < 0 ? 0 : passedScroll > 1 ? 1 : passedScroll;
    cups[2].style.opacity = 1 - passedScroll * 1.4;
    if (cups[2].style.opacity <= 0) {
        cups[1].style.opacity = 1 - passedScroll;
    }
});

//phone mask
let phoneInputs = document.querySelectorAll('[type=tel]');
let maskOptions = {
    mask: '+{7}(000)000-00-00', //pattern
    lazy: false
};
for (let input of phoneInputs) {
    let mask = new IMask(input, maskOptions);
    input.classList.toggle('not-empty', true);
}

//label move on focus and input
let inputs = document.querySelectorAll('input');
for (let input of inputs) {
    input.addEventListener('input', function () {
        this.classList.toggle('not-empty', this.value.length > 0);
        this.setCustomValidity("");
    });
}


//form ajax submit
$('form').submit(function (e) {
    e.preventDefault();
    let phoneInput = this.querySelector('[type=tel]');
    if (phoneInput && phoneInput.value.replace(/[^0-9\.]+/g, "").length < 11) {
        phoneInput.setCustomValidity("Поле заполнено неправильно");
        phoneInput.reportValidity();
        return false;
    }

    let thankYou = $(`<h2 style="text-align: center;">Спасибо! Ваша заявка отправлена. Мы скоро вам перезвоним.</h2>`);
    let $form = $(this);
    $form.find('button[type=submit]').prop('disabled', true);

    let form_data = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "/email.php",
        data: form_data,
        success: function (data) {
            console.log('success');
            $form.replaceWith(thankYou);
        },
        error: function (data) {
            console.log(data);
        }
    });
});

