function CustomMarker(latlng, map, args) {
    this.latlng = latlng;
    this.args = args;
    this.setMap(map);
}

function initMarkers() {
    CustomMarker.prototype = new google.maps.OverlayView();

    CustomMarker.prototype.draw = function () {

        var self = this;

        var div = this.div;

        if (!div) {

            div = this.div = document.createElement('div');
            if (typeof(self.args.text) !== 'undefined') {
                div.dataset.text = self.args.text;
            }

            div.className = 'custom-marker';

            div.style.position = 'absolute';
            div.style.cursor = 'pointer';

            google.maps.event.addDomListener(div, "mouseover", function (event) {
                div.innerText = this.dataset.text;
            });

            google.maps.event.addDomListener(div, "mouseout", function (event) {
                div.innerText = "";
            });

            var panes = this.getPanes();
            panes.overlayImage.appendChild(div);
        }

        var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

        if (point) {
            div.style.left = point.x + 'px';
            div.style.top = point.y + 'px';
        }
    };

    CustomMarker.prototype.remove = function () {
        if (this.div) {
            this.div.parentNode.removeChild(this.div);
            this.div = null;
        }
    };

    CustomMarker.prototype.getPosition = function () {
        return this.latlng;
    };
}

export function initMap() {
    initMarkers();
    let map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 55.8678134, lng: 37.6624845},
        zoom: 13
    });

    let shops = [
        {
            position: new google.maps.LatLng(55.8678134, 37.6624845),
            text: "ул. Ленина, д. 47"
        },
        {
            position: new google.maps.LatLng(55.860353, 37.6695363),
            text: "ул. Измайлова, д. 10, корп. 1"
        },
    ];

    for (let i = 0; i < shops.length; i++) {
        new CustomMarker(
            shops[i].position,
            map,
            {text: shops[i].text}
        );
    }
}
