webpackJsonp([1],[
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(7);
__webpack_require__(12);
module.exports = __webpack_require__(13);


/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__map__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_bootstrap_js_dist_tab__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_bootstrap_js_dist_tab___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_bootstrap_js_dist_tab__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_aos_dist_aos_js__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_aos_dist_aos_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_aos_dist_aos_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_imask__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_imask___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_imask__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jquery__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jquery___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_jquery__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_swiper__ = __webpack_require__(4);





window.$ = window.jQuery = __WEBPACK_IMPORTED_MODULE_4_jquery___default.a;


//init google map
window.initMap = __WEBPACK_IMPORTED_MODULE_0__map__["a" /* initMap */];

//sliders initialization
var swipers = document.querySelectorAll('.swiper-container');
var _iteratorNormalCompletion = true;
var _didIteratorError = false;
var _iteratorError = undefined;

try {
    for (var _iterator = swipers[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var element = _step.value;

        new __WEBPACK_IMPORTED_MODULE_5_swiper__["default"](element, {
            effect: 'coverflow',
            grabCursor: true,
            centeredSlides: true,
            slidesPerView: 'auto',
            coverflowEffect: {
                rotate: 0,
                stretch: 0,
                depth: 200,
                modifier: 1
            },
            observer: true,
            observeParents: true,
            navigation: {
                nextEl: element.querySelector('.navigation--right'),
                prevEl: element.querySelector('.navigation--left')
            },
            breakpoints: {
                767: {
                    slidesPerView: 1,
                    spaceBetween: 10
                }
            }
        });
    }

    //scroll animations initialization
} catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
} finally {
    try {
        if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
        }
    } finally {
        if (_didIteratorError) {
            throw _iteratorError;
        }
    }
}

__WEBPACK_IMPORTED_MODULE_2_aos_dist_aos_js___default.a.init();

//attach background to mouse
var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;
var bgSplash = $('.bg-splash__image-bg');

function moveBackground() {
    x += (lFollowX - x) * friction;
    y += (lFollowY - y) * friction;

    var translate = 'translate(' + x + 'px, ' + y + 'px) scale(1.1)';

    bgSplash.css({
        '-webit-transform': translate,
        '-moz-transform': translate,
        'transform': translate
    });

    window.requestAnimationFrame(moveBackground);
}

$(window).on('mousemove click', function (e) {
    var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
    var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
    lFollowX = 20 * lMouseX / 100;
    lFollowY = 10 * lMouseY / 100;
});
moveBackground();

//cup animation
var cups = $('.cup__layers img');
var cupOffsetY = $(cups[1]).offset().top;

$(window).on('scroll', function (e) {
    var passedScroll = 1 - (cupOffsetY - window.scrollY) / window.outerHeight;
    passedScroll = passedScroll < 0 ? 0 : passedScroll > 1 ? 1 : passedScroll;
    cups[2].style.opacity = 1 - passedScroll * 1.4;
    if (cups[2].style.opacity <= 0) {
        cups[1].style.opacity = 1 - passedScroll;
    }
});

//phone mask
var phoneInputs = document.querySelectorAll('[type=tel]');
var maskOptions = {
    mask: '+{7}(000)000-00-00', //pattern
    lazy: false
};
var _iteratorNormalCompletion2 = true;
var _didIteratorError2 = false;
var _iteratorError2 = undefined;

try {
    for (var _iterator2 = phoneInputs[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
        var input = _step2.value;

        var mask = new __WEBPACK_IMPORTED_MODULE_3_imask___default.a(input, maskOptions);
        input.classList.toggle('not-empty', true);
    }

    //label move on focus and input
} catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
} finally {
    try {
        if (!_iteratorNormalCompletion2 && _iterator2.return) {
            _iterator2.return();
        }
    } finally {
        if (_didIteratorError2) {
            throw _iteratorError2;
        }
    }
}

var inputs = document.querySelectorAll('input');
var _iteratorNormalCompletion3 = true;
var _didIteratorError3 = false;
var _iteratorError3 = undefined;

try {
    for (var _iterator3 = inputs[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
        var _input = _step3.value;

        _input.addEventListener('input', function () {
            this.classList.toggle('not-empty', this.value.length > 0);
            this.setCustomValidity("");
        });
    }

    //form ajax submit
} catch (err) {
    _didIteratorError3 = true;
    _iteratorError3 = err;
} finally {
    try {
        if (!_iteratorNormalCompletion3 && _iterator3.return) {
            _iterator3.return();
        }
    } finally {
        if (_didIteratorError3) {
            throw _iteratorError3;
        }
    }
}

$('form').submit(function (e) {
    e.preventDefault();
    var phoneInput = this.querySelector('[type=tel]');
    if (phoneInput && phoneInput.value.replace(/[^0-9\.]+/g, "").length < 11) {
        phoneInput.setCustomValidity("Поле заполнено неправильно");
        phoneInput.reportValidity();
        return false;
    }

    var thankYou = $('<h2 style="text-align: center;">\u0421\u043F\u0430\u0441\u0438\u0431\u043E! \u0412\u0430\u0448\u0430 \u0437\u0430\u044F\u0432\u043A\u0430 \u043E\u0442\u043F\u0440\u0430\u0432\u043B\u0435\u043D\u0430. \u041C\u044B \u0441\u043A\u043E\u0440\u043E \u0432\u0430\u043C \u043F\u0435\u0440\u0435\u0437\u0432\u043E\u043D\u0438\u043C.</h2>');
    var $form = $(this);
    $form.find('button[type=submit]').prop('disabled', true);

    var form_data = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "/email.php",
        data: form_data,
        success: function success(data) {
            console.log('success');
            $form.replaceWith(thankYou);
        },
        error: function error(data) {
            console.log(data);
        }
    });
});

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = initMap;
function CustomMarker(latlng, map, args) {
    this.latlng = latlng;
    this.args = args;
    this.setMap(map);
}

function initMarkers() {
    CustomMarker.prototype = new google.maps.OverlayView();

    CustomMarker.prototype.draw = function () {

        var self = this;

        var div = this.div;

        if (!div) {

            div = this.div = document.createElement('div');
            if (typeof self.args.text !== 'undefined') {
                div.dataset.text = self.args.text;
            }

            div.className = 'custom-marker';

            div.style.position = 'absolute';
            div.style.cursor = 'pointer';

            google.maps.event.addDomListener(div, "mouseover", function (event) {
                div.innerText = this.dataset.text;
            });

            google.maps.event.addDomListener(div, "mouseout", function (event) {
                div.innerText = "";
            });

            var panes = this.getPanes();
            panes.overlayImage.appendChild(div);
        }

        var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

        if (point) {
            div.style.left = point.x + 'px';
            div.style.top = point.y + 'px';
        }
    };

    CustomMarker.prototype.remove = function () {
        if (this.div) {
            this.div.parentNode.removeChild(this.div);
            this.div = null;
        }
    };

    CustomMarker.prototype.getPosition = function () {
        return this.latlng;
    };
}

function initMap() {
    initMarkers();
    var map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 55.8678134, lng: 37.6624845 },
        zoom: 13
    });

    var shops = [{
        position: new google.maps.LatLng(55.8678134, 37.6624845),
        text: "ул. Ленина, д. 47"
    }, {
        position: new google.maps.LatLng(55.860353, 37.6695363),
        text: "ул. Измайлова, д. 10, корп. 1"
    }];

    for (var i = 0; i < shops.length; i++) {
        new CustomMarker(shops[i].position, map, { text: shops[i].text });
    }
}

/***/ }),
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 13 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
],[6]);