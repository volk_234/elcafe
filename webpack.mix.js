let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .options({
        processCssUrls: false
    })
    .setPublicPath('public')
    .copyDirectory('src/images', 'public/images')
    .copyDirectory('src/fonts', 'public/fonts');

mix.sass('src/sass/app.scss', 'public/css', { outputStyle: 'nested' });
mix.sass('src/sass/vendor.scss', 'public/css');

mix.js('src/js/app.js', 'public/js')
    .extract(['bootstrap/js/dist/tab', 'aos', 'imask', 'jquery', 'swiper']);
